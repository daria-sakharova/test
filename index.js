const fetch = require('node-fetch');

//reduce
/*
const urls = [];
for (let i = 1; i <= 100; i++){
    urls.push(`https://jsonplaceholder.typicode.com/posts/${i}`);
}
urls.reduce( (reducer, url) => {
    return reducer.then(
        Promise.all( urls.splice(0,5).map( url => fetch(url).then(res => res.json()) ))
            .then(posts => posts.forEach( (item) => console.log(item) ) )
    )
}, Promise.resolve());
*/

//recursion, no for-loop
const mainUrl = 'https://jsonplaceholder.typicode.com/posts/';
const step = 5;
const max = 100;
let current = 0;
let promiseChain = Promise.resolve()

function addPromises() {
    promiseChain = promiseChain.then(
        Promise.all( new Array(step).fill(1).map( (item, i, arr) => i+1)
            .map( item => fetch(`${mainUrl}${current+item}`).then(res => res.json()) )
        ).then(posts => posts.forEach( (item) => console.log(item) ) ).catch( err => {console.log(err)})
    )
    current += 5;
    if (current !== max){
        addPromises();
    }
}

addPromises();